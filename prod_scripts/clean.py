import numpy as np
import pandas as pd

print("Loading File...")
# file_str = "tsopp-mt-QueryResult.csv"
file_str = "works.csv"
df = pd.read_csv(file_str, encoding='utf8')
print("File loaded!")

#Encode the data into ascii
# print("Replacing non-ascii characters...")
# columns = list(df)
# for col in columns:
#     df[col].replace({r'[^\x00-\x7F]+':''}, regex=True, inplace=True)

print("Replacing blanks...")
#Replace blank values with None
df.replace('', np.nan, inplace=True)
df = df.replace({pd.np.nan:None})

#Get the frame in 1nf by adding rows for multiple values in violation column
df1 = df.assign(violation=df.violation.str.split(',')).explode('violation').reset_index(inplace=False)
df1 = df1.drop(columns='index') #Remove the extra index column
df1.index.name = 'record_id' #Rename the new index column to 'index'
df1.columns = ['stop_id', 'stop_date', 'stop_time', 'stop_outcome' ,'violation',
               'vehicle_year', 'vehicle_make', 'vehicle_model', 'vehicle_style',
               'driver_gender', 'driver_age','driver_race_raw','out_of_state']
df1.to_csv("testOut.csv")

#Get the frame into 2nf
