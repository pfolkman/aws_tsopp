COPY violation_details(violation_id,violation_type) 
FROM 'C:\Users\pfolkman\Desktop\Parkers_School_Docs\Montana_State\Database\project\aws_tsopp\prod_scripts\violation_details.csv' DELIMITER ',' CSV HEADER;

COPY driver_details(stop_id,driver_gender,driver_age,driver_race,out_of_state) 
FROM 'C:\Users\pfolkman\Desktop\Parkers_School_Docs\Montana_State\Database\project\aws_tsopp\prod_scripts\driver_details.csv' DELIMITER ',' CSV HEADER;

COPY vehicle_details(stop_id,vehicle_year,vehicle_make,vehicle_model,vehicle_style) 
FROM 'C:\Users\pfolkman\Desktop\Parkers_School_Docs\Montana_State\Database\project\aws_tsopp\prod_scripts\vehicle_details.csv' DELIMITER ',' CSV HEADER;

COPY stop_info(record_id,stop_id,stop_date,stop_time,stop_outcome,violation_id) 
FROM 'C:\Users\pfolkman\Desktop\Parkers_School_Docs\Montana_State\Database\project\aws_tsopp\prod_scripts\stop_info.csv' DELIMITER ',' CSV HEADER;