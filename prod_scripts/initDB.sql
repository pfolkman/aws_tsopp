drop table if exists stop_info cascade;
drop table if exists vehicle_details cascade;
drop table if exists violation_details cascade;
drop table if exists driver_details cascade;

--Create Schema
create table violation_details(
	violation_id char(4) primary key unique,
	violation_type text
);

create table vehicle_details(
	stop_id char(14) primary key unique,
	vehicle_year char(4),
	vehicle_make varchar(24),
	vehicle_model varchar(24),
	vehicle_style varchar(24)
);

create table driver_details(
	stop_id char(14) primary key,
	driver_gender char(1),
	driver_age smallint,
	driver_race varchar(24),
	out_of_state boolean not null
);

--Create Traffic_stop table
create table stop_info(
	record_id serial primary key unique,
	stop_id char(14) not null,
	stop_date date not null,
	stop_time time,
	stop_outcome varchar(24),
	--violation_id char(4) references violation_details,
	violation_id char(4) not null,
	vehicle_make varchar(24),
	foreign key (stop_id) references vehicle_details (stop_id),
	foreign key (stop_id) references driver_details (stop_id),
	foreign key (violation_id) references violation_details (violation_id),
	constraint chk_date check (stop_date < current_date)
);

--create index for searching by stop_id
create index stop_id_index
	on stop_info using btree (stop_id);
	
--create index for searching by violation type
create index violation_type_index
	on stop_info using btree (violation_id);