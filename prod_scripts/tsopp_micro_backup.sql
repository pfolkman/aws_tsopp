--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

-- Started on 2019-10-31 17:43:52

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 196 (class 1259 OID 16497)
-- Name: driver_details; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.driver_details (
    stop_id character(14) NOT NULL,
    driver_gender character(1),
    driver_age smallint,
    driver_race character varying(24),
    out_of_state boolean NOT NULL
);


ALTER TABLE public.driver_details OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 16500)
-- Name: stop_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.stop_info (
    record_id integer NOT NULL,
    stop_id character(14) NOT NULL,
    stop_date date NOT NULL,
    stop_time time without time zone,
    stop_outcome character varying(24),
    violation_id character(4),
    CONSTRAINT chk_date CHECK ((stop_date < CURRENT_DATE))
);


ALTER TABLE public.stop_info OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 16504)
-- Name: stop_info_record_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.stop_info_record_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stop_info_record_id_seq OWNER TO postgres;

--
-- TOC entry 3837 (class 0 OID 0)
-- Dependencies: 198
-- Name: stop_info_record_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.stop_info_record_id_seq OWNED BY public.stop_info.record_id;


--
-- TOC entry 199 (class 1259 OID 16506)
-- Name: vehicle_details; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.vehicle_details (
    stop_id character(14) NOT NULL,
    vehicle_year character(4),
    vehicle_make character varying(24),
    vehicle_model character varying(24),
    vehicle_style character varying(24)
);


ALTER TABLE public.vehicle_details OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 16509)
-- Name: violation_details; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.violation_details (
    violation_id character(4) NOT NULL,
    violation_type text
);


ALTER TABLE public.violation_details OWNER TO postgres;

--
-- TOC entry 3693 (class 2604 OID 16515)
-- Name: stop_info record_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stop_info ALTER COLUMN record_id SET DEFAULT nextval('public.stop_info_record_id_seq'::regclass);


--
-- TOC entry 3826 (class 0 OID 16497)
-- Dependencies: 196
-- Data for Name: driver_details; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000313', 'M', 19, 'Native American', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000314', 'M', 20, 'Unknown', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000315', 'M', 21, 'Black', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000316', 'F', 25, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000317', 'M', 20, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000318', 'M', 44, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000319', 'M', 64, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000320', 'M', 23, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000321', 'M', 56, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000322', 'M', 23, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000323', 'M', 25, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000324', 'M', 50, 'Native American', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000325', 'M', 24, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000326', 'M', 60, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000327', 'M', 28, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000328', 'M', 60, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000329', 'M', 42, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000330', 'M', 18, 'Native American', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000331', 'F', 21, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000332', 'M', 37, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000333', 'F', 33, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000334', 'M', 19, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000335', 'M', 52, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000336', 'M', 19, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000337', 'M', 31, 'Hispanic', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000338', 'M', 83, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000339', 'M', 29, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000340', 'M', 40, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000341', 'M', 54, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000342', 'M', 57, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000343', 'M', 31, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000344', 'M', 57, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000345', 'M', 65, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000346', 'M', 54, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000347', 'M', 17, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000348', 'M', 40, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000349', 'M', 24, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000350', 'M', 30, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000351', 'M', 27, 'Asian / Pacific Islander', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000352', 'M', 55, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000353', 'M', 32, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000354', 'F', 19, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000355', 'M', 18, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000356', 'M', 30, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000357', 'F', 40, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000358', 'M', 34, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000359', 'M', 69, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000360', 'M', 52, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000361', 'M', 42, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000362', 'F', 63, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000363', 'F', 57, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000364', 'M', 41, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000365', 'M', 33, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000366', 'M', 27, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000367', 'M', 25, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000368', 'F', 38, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000369', 'M', 19, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000370', 'M', 68, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000371', 'M', 36, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000372', 'F', 43, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000373', 'F', 73, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000374', 'M', 24, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000375', 'M', 45, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000376', 'M', 28, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000377', 'F', 30, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000378', 'M', 72, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000379', 'M', 30, 'Hispanic', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000380', 'M', 68, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000381', 'F', 25, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000382', 'M', 35, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000383', 'F', 36, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000384', 'M', 31, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000385', 'M', 37, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000386', 'M', 64, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000387', 'M', 56, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000388', 'M', 62, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000389', 'F', 35, 'Native American', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000390', 'M', 28, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000391', 'M', 16, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000392', 'F', 21, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000393', 'M', 58, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000394', 'F', 38, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000395', 'M', 61, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000396', 'M', 62, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000397', 'M', 31, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000398', 'M', 16, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000399', 'M', 44, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000400', 'M', 47, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000401', 'F', 44, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000402', 'M', 31, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000403', 'F', 31, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000404', 'M', 64, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000405', 'M', 69, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000406', 'F', 29, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000407', 'M', 45, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000408', 'M', 40, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000409', 'F', 17, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000410', 'F', 49, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000411', 'M', 25, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000412', 'M', 85, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000413', 'M', 31, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000414', 'F', 70, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000415', 'F', 33, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000416', 'F', 44, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000417', 'M', 26, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000418', 'F', 23, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000419', 'M', 27, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000420', 'M', 30, 'Native American', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000421', 'M', 45, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000422', 'M', 27, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000423', 'F', 49, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000424', 'M', 29, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000425', 'F', 35, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000426', 'F', 51, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000427', 'M', 51, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000428', 'M', 18, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000429', 'M', 51, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000430', 'M', 37, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000431', 'F', 36, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000432', 'M', 31, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000433', 'F', 54, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000434', 'M', 34, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000435', 'M', 28, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000436', 'M', 53, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000437', 'M', 69, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000438', 'M', 62, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000439', 'M', 18, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000440', 'M', 57, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000441', 'M', 32, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000442', 'M', 19, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000443', 'M', 17, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000444', 'M', 61, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000445', 'M', 31, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000446', 'M', 31, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000447', 'F', 31, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000448', 'M', 27, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000449', 'F', 39, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000450', 'M', 27, 'Asian / Pacific Islander', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000451', 'F', 40, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000452', 'F', 53, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000453', 'M', 44, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000454', 'F', 35, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000455', 'M', 21, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000456', 'F', 50, 'Native American', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000457', 'M', 44, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000458', 'F', 21, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000459', 'F', 18, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000460', 'F', 31, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000461', 'M', 58, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000462', 'M', 58, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000463', 'M', 36, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000464', 'M', 58, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000465', 'M', 39, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000466', 'F', 35, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000467', 'M', 26, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000468', 'F', 58, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000469', 'M', 50, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000470', 'M', 65, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000471', 'M', 49, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000472', 'M', 27, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000473', 'F', 22, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000474', 'F', 57, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000475', 'M', 35, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000476', 'M', 31, 'Native American', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000477', 'F', 59, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000478', 'M', 37, 'Native American', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000479', 'M', 81, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000480', 'M', 68, 'Native American', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000481', 'M', 47, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000482', 'M', 39, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000483', 'M', 43, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000484', 'M', 38, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000485', 'F', 26, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000486', 'M', 31, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000487', 'M', 22, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000488', 'M', 28, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000489', 'F', 66, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000490', 'M', 28, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000491', 'M', 55, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000492', 'M', 37, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000493', 'M', 68, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000494', 'M', 54, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000495', 'M', 44, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000496', 'M', 35, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000497', 'M', 49, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000498', 'F', 36, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000499', 'F', 35, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000500', 'M', 20, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000501', 'M', 49, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000502', 'M', 54, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000503', 'M', 18, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000504', 'M', 37, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000505', 'F', 24, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000506', 'M', 44, 'Native American', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000507', 'M', 42, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000508', 'M', 41, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000509', 'M', 53, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000510', 'M', 30, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000511', 'F', 22, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000512', 'F', 59, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000513', 'F', 60, 'Native American', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000514', 'M', 67, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000515', 'M', 22, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000516', 'M', 22, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000517', 'M', 23, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000518', 'F', 29, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000519', 'F', 45, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000520', 'F', 37, 'Native American', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000521', 'M', 21, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000522', 'M', 54, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000523', 'F', 67, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000524', 'M', 56, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000525', 'F', 53, 'Native American', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000526', 'F', 51, 'Native American', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000527', 'M', 54, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000528', 'M', 20, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000529', 'M', 28, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000530', 'M', 29, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000531', 'F', 45, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000532', 'M', 26, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000533', 'F', 18, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000534', 'M', 33, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000535', 'F', 30, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000536', 'F', 35, 'Hispanic', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000537', 'M', 57, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000538', 'M', 37, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000539', 'F', 54, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000540', 'M', 31, 'Asian / Pacific Islander', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000541', 'M', 21, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000542', 'M', 33, 'Hispanic', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000543', 'M', 41, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000544', 'M', 69, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000545', 'F', 24, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000546', 'M', 39, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000547', 'M', 24, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000548', 'F', 21, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000549', 'F', 55, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000550', 'M', 39, 'Hispanic', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000551', 'M', 51, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000552', 'F', 30, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000553', 'M', 27, 'Native American', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000554', 'M', 37, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000555', 'M', 62, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000556', 'F', 28, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000557', 'M', 52, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000558', 'M', 34, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000559', 'M', 38, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000560', 'F', 18, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000561', 'M', 18, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000562', 'F', 21, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000563', 'M', 36, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000564', 'M', 40, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000565', 'F', 46, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000566', 'M', 50, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000567', 'F', 21, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000568', 'M', 25, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000569', 'M', 36, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000570', 'M', 36, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000571', 'M', 21, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000572', 'F', 47, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000573', 'M', 51, 'Native American', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000574', 'F', 35, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000575', 'M', 25, 'Black', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000576', 'M', 24, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000577', 'M', 27, 'Hispanic', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000578', 'F', 41, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000579', 'F', 38, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000580', 'F', 61, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000581', 'M', 54, 'Native American', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000582', 'M', 29, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000583', 'M', 53, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000584', 'F', 17, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000585', 'M', 20, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000586', 'M', 18, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000587', 'M', 37, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000588', 'M', 42, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000589', 'M', 23, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000590', 'F', 32, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000591', 'M', 21, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000592', 'M', 18, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000593', 'M', 23, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000594', 'F', NULL, 'Asian / Pacific Islander', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000595', 'M', 35, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000596', 'M', 79, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000597', 'F', 80, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000598', 'M', 53, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000599', 'F', 23, 'Black', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000600', 'M', 36, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000601', 'M', 22, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000602', 'M', 27, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000603', 'M', 43, 'White', false);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000604', 'M', 43, 'White', true);
INSERT INTO public.driver_details (stop_id, driver_gender, driver_age, driver_race, out_of_state) VALUES ('MT-2016-000605', 'M', 46, 'White', false);


--
-- TOC entry 3827 (class 0 OID 16500)
-- Dependencies: 197
-- Data for Name: stop_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (0, 'MT-2016-000313', '2016-01-02', '00:34:00', 'Citation', '1000');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (1, 'MT-2016-000313', '2016-01-02', '00:34:00', 'Citation', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (2, 'MT-2016-000313', '2016-01-02', '00:34:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (3, 'MT-2016-000314', '2016-01-02', '00:40:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (4, 'MT-2016-000314', '2016-01-02', '00:40:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (5, 'MT-2016-000315', '2016-01-02', '00:46:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (6, 'MT-2016-000315', '2016-01-02', '00:46:00', 'Citation', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (7, 'MT-2016-000315', '2016-01-02', '00:46:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (8, 'MT-2016-000316', '2016-01-02', '00:51:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (9, 'MT-2016-000316', '2016-01-02', '00:51:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (10, 'MT-2016-000317', '2016-01-02', '01:03:00', 'Warning', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (11, 'MT-2016-000317', '2016-01-02', '01:03:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (12, 'MT-2016-000317', '2016-01-02', '01:03:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (13, 'MT-2016-000318', '2016-01-02', '01:08:00', 'Citation', '1000');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (14, 'MT-2016-000318', '2016-01-02', '01:08:00', 'Citation', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (15, 'MT-2016-000318', '2016-01-02', '01:08:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (16, 'MT-2016-000319', '2016-01-02', '01:12:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (17, 'MT-2016-000319', '2016-01-02', '01:12:00', 'Citation', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (18, 'MT-2016-000320', '2016-01-02', '01:13:00', 'Warning', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (19, 'MT-2016-000320', '2016-01-02', '01:13:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (20, 'MT-2016-000321', '2016-01-02', '01:32:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (21, 'MT-2016-000322', '2016-01-02', '01:36:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (22, 'MT-2016-000322', '2016-01-02', '01:36:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (23, 'MT-2016-000323', '2016-01-02', '01:44:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (24, 'MT-2016-000324', '2016-01-02', '01:49:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (25, 'MT-2016-000324', '2016-01-02', '01:49:00', 'Citation', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (26, 'MT-2016-000325', '2016-01-02', '02:00:00', 'Warning', '1007');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (27, 'MT-2016-000326', '2016-01-02', '02:04:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (28, 'MT-2016-000327', '2016-01-02', '02:30:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (29, 'MT-2016-000328', '2016-01-02', '02:41:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (30, 'MT-2016-000328', '2016-01-02', '02:41:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (31, 'MT-2016-000329', '2016-01-02', '02:42:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (32, 'MT-2016-000329', '2016-01-02', '02:42:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (33, 'MT-2016-000330', '2016-01-02', '02:43:00', 'Warning', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (34, 'MT-2016-000330', '2016-01-02', '02:43:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (35, 'MT-2016-000331', '2016-01-02', '07:19:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (36, 'MT-2016-000332', '2016-01-02', '07:24:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (37, 'MT-2016-000332', '2016-01-02', '07:24:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (38, 'MT-2016-000333', '2016-01-02', '07:37:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (39, 'MT-2016-000334', '2016-01-02', '07:38:00', 'Arrest', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (40, 'MT-2016-000335', '2016-01-02', '07:40:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (41, 'MT-2016-000336', '2016-01-02', '07:47:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (42, 'MT-2016-000337', '2016-01-02', '07:50:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (43, 'MT-2016-000337', '2016-01-02', '07:50:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (44, 'MT-2016-000338', '2016-01-02', '07:52:00', 'Warning', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (45, 'MT-2016-000338', '2016-01-02', '07:52:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (46, 'MT-2016-000339', '2016-01-02', '07:52:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (47, 'MT-2016-000339', '2016-01-02', '07:52:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (48, 'MT-2016-000340', '2016-01-02', '07:53:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (49, 'MT-2016-000341', '2016-01-02', '07:59:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (50, 'MT-2016-000342', '2016-01-02', '07:59:00', 'Arrest', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (51, 'MT-2016-000343', '2016-01-02', '08:02:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (52, 'MT-2016-000344', '2016-01-02', '08:02:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (53, 'MT-2016-000344', '2016-01-02', '08:02:00', 'Warning', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (54, 'MT-2016-000344', '2016-01-02', '08:02:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (55, 'MT-2016-000345', '2016-01-02', '08:03:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (56, 'MT-2016-000346', '2016-01-02', '08:09:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (57, 'MT-2016-000347', '2016-01-02', '08:16:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (58, 'MT-2016-000347', '2016-01-02', '08:16:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (59, 'MT-2016-000347', '2016-01-02', '08:16:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (60, 'MT-2016-000348', '2016-01-02', '08:17:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (61, 'MT-2016-000349', '2016-01-02', '08:18:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (62, 'MT-2016-000349', '2016-01-02', '08:18:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (63, 'MT-2016-000350', '2016-01-02', '08:21:00', 'Warning', '1010');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (64, 'MT-2016-000351', '2016-01-02', '08:38:00', 'Arrest', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (65, 'MT-2016-000352', '2016-01-02', '08:43:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (66, 'MT-2016-000352', '2016-01-02', '08:43:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (67, 'MT-2016-000352', '2016-01-02', '08:43:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (68, 'MT-2016-000353', '2016-01-02', '08:49:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (69, 'MT-2016-000354', '2016-01-02', '08:51:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (70, 'MT-2016-000355', '2016-01-02', '08:52:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (71, 'MT-2016-000356', '2016-01-02', '08:55:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (72, 'MT-2016-000356', '2016-01-02', '08:55:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (73, 'MT-2016-000357', '2016-01-02', '08:59:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (74, 'MT-2016-000357', '2016-01-02', '08:59:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (75, 'MT-2016-000357', '2016-01-02', '08:59:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (76, 'MT-2016-000358', '2016-01-02', '08:59:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (77, 'MT-2016-000359', '2016-01-02', '09:15:00', 'Citation', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (78, 'MT-2016-000359', '2016-01-02', '09:15:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (79, 'MT-2016-000360', '2016-01-02', '09:16:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (80, 'MT-2016-000360', '2016-01-02', '09:16:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (81, 'MT-2016-000361', '2016-01-02', '09:20:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (82, 'MT-2016-000361', '2016-01-02', '09:20:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (83, 'MT-2016-000362', '2016-01-02', '09:26:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (84, 'MT-2016-000362', '2016-01-02', '09:26:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (85, 'MT-2016-000363', '2016-01-02', '09:26:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (86, 'MT-2016-000363', '2016-01-02', '09:26:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (87, 'MT-2016-000364', '2016-01-02', '09:27:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (88, 'MT-2016-000365', '2016-01-02', '09:31:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (89, 'MT-2016-000365', '2016-01-02', '09:31:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (90, 'MT-2016-000366', '2016-01-02', '09:35:00', 'Warning', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (91, 'MT-2016-000366', '2016-01-02', '09:35:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (92, 'MT-2016-000367', '2016-01-02', '09:37:00', 'Warning', '1007');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (93, 'MT-2016-000368', '2016-01-02', '09:38:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (94, 'MT-2016-000369', '2016-01-02', '09:38:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (95, 'MT-2016-000369', '2016-01-02', '09:38:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (96, 'MT-2016-000370', '2016-01-02', '09:38:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (97, 'MT-2016-000371', '2016-01-02', '09:39:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (98, 'MT-2016-000372', '2016-01-02', '09:42:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (99, 'MT-2016-000372', '2016-01-02', '09:42:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (100, 'MT-2016-000372', '2016-01-02', '09:42:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (101, 'MT-2016-000373', '2016-01-02', '09:46:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (102, 'MT-2016-000373', '2016-01-02', '09:46:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (103, 'MT-2016-000374', '2016-01-02', '09:56:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (104, 'MT-2016-000374', '2016-01-02', '09:56:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (105, 'MT-2016-000375', '2016-01-02', '09:57:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (106, 'MT-2016-000375', '2016-01-02', '09:57:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (107, 'MT-2016-000376', '2016-01-02', '09:59:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (108, 'MT-2016-000377', '2016-01-02', '09:59:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (109, 'MT-2016-000378', '2016-01-02', '10:00:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (110, 'MT-2016-000379', '2016-01-02', '10:00:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (111, 'MT-2016-000379', '2016-01-02', '10:00:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (112, 'MT-2016-000380', '2016-01-02', '10:02:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (113, 'MT-2016-000381', '2016-01-02', '10:07:00', 'Citation', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (114, 'MT-2016-000381', '2016-01-02', '10:07:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (115, 'MT-2016-000381', '2016-01-02', '10:07:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (116, 'MT-2016-000382', '2016-01-02', '10:12:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (117, 'MT-2016-000383', '2016-01-02', '10:12:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (118, 'MT-2016-000383', '2016-01-02', '10:12:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (119, 'MT-2016-000383', '2016-01-02', '10:12:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (120, 'MT-2016-000384', '2016-01-02', '10:14:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (121, 'MT-2016-000384', '2016-01-02', '10:14:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (122, 'MT-2016-000385', '2016-01-02', '10:14:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (123, 'MT-2016-000385', '2016-01-02', '10:14:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (124, 'MT-2016-000385', '2016-01-02', '10:14:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (125, 'MT-2016-000386', '2016-01-02', '10:19:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (126, 'MT-2016-000386', '2016-01-02', '10:19:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (127, 'MT-2016-000386', '2016-01-02', '10:19:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (128, 'MT-2016-000387', '2016-01-02', '10:24:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (129, 'MT-2016-000388', '2016-01-02', '10:26:00', 'Citation', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (130, 'MT-2016-000388', '2016-01-02', '10:26:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (131, 'MT-2016-000388', '2016-01-02', '10:26:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (132, 'MT-2016-000389', '2016-01-02', '10:27:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (133, 'MT-2016-000390', '2016-01-02', '10:33:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (134, 'MT-2016-000391', '2016-01-02', '10:34:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (135, 'MT-2016-000392', '2016-01-02', '10:35:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (136, 'MT-2016-000392', '2016-01-02', '10:35:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (137, 'MT-2016-000392', '2016-01-02', '10:35:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (138, 'MT-2016-000393', '2016-01-02', '10:35:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (139, 'MT-2016-000393', '2016-01-02', '10:35:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (140, 'MT-2016-000394', '2016-01-02', '10:40:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (141, 'MT-2016-000395', '2016-01-02', '10:42:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (142, 'MT-2016-000396', '2016-01-02', '10:44:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (143, 'MT-2016-000397', '2016-01-02', '10:46:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (144, 'MT-2016-000398', '2016-01-02', '10:46:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (145, 'MT-2016-000398', '2016-01-02', '10:46:00', 'Warning', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (146, 'MT-2016-000399', '2016-01-02', '10:50:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (147, 'MT-2016-000399', '2016-01-02', '10:50:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (148, 'MT-2016-000400', '2016-01-02', '10:54:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (149, 'MT-2016-000401', '2016-01-02', '10:55:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (150, 'MT-2016-000402', '2016-01-02', '10:57:00', 'Warning', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (151, 'MT-2016-000403', '2016-01-02', '11:03:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (152, 'MT-2016-000403', '2016-01-02', '11:03:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (153, 'MT-2016-000403', '2016-01-02', '11:03:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (154, 'MT-2016-000404', '2016-01-02', '11:04:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (155, 'MT-2016-000405', '2016-01-02', '11:05:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (156, 'MT-2016-000406', '2016-01-02', '11:08:00', 'Warning', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (157, 'MT-2016-000406', '2016-01-02', '11:08:00', 'Warning', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (158, 'MT-2016-000407', '2016-01-02', '11:12:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (159, 'MT-2016-000408', '2016-01-02', '11:17:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (160, 'MT-2016-000409', '2016-01-02', '11:18:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (161, 'MT-2016-000410', '2016-01-02', '11:19:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (162, 'MT-2016-000411', '2016-01-02', '11:22:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (163, 'MT-2016-000411', '2016-01-02', '11:22:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (164, 'MT-2016-000411', '2016-01-02', '11:22:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (165, 'MT-2016-000412', '2016-01-02', '11:25:00', 'Arrest', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (166, 'MT-2016-000412', '2016-01-02', '11:25:00', 'Arrest', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (167, 'MT-2016-000413', '2016-01-02', '11:25:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (168, 'MT-2016-000414', '2016-01-02', '11:28:00', 'Citation', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (169, 'MT-2016-000414', '2016-01-02', '11:28:00', 'Citation', '1007');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (170, 'MT-2016-000415', '2016-01-02', '11:30:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (171, 'MT-2016-000416', '2016-01-02', '11:32:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (172, 'MT-2016-000417', '2016-01-02', '11:35:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (173, 'MT-2016-000417', '2016-01-02', '11:35:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (174, 'MT-2016-000418', '2016-01-02', '11:39:00', 'Arrest', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (175, 'MT-2016-000418', '2016-01-02', '11:39:00', 'Arrest', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (176, 'MT-2016-000418', '2016-01-02', '11:39:00', 'Arrest', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (177, 'MT-2016-000419', '2016-01-02', '11:40:00', 'Citation', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (178, 'MT-2016-000419', '2016-01-02', '11:40:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (179, 'MT-2016-000419', '2016-01-02', '11:40:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (180, 'MT-2016-000420', '2016-01-02', '11:46:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (181, 'MT-2016-000420', '2016-01-02', '11:46:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (182, 'MT-2016-000421', '2016-01-02', '11:47:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (183, 'MT-2016-000421', '2016-01-02', '11:47:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (184, 'MT-2016-000422', '2016-01-02', '11:50:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (185, 'MT-2016-000422', '2016-01-02', '11:50:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (186, 'MT-2016-000422', '2016-01-02', '11:50:00', 'Citation', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (187, 'MT-2016-000423', '2016-01-02', '11:51:00', 'Arrest', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (188, 'MT-2016-000424', '2016-01-02', '11:52:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (189, 'MT-2016-000424', '2016-01-02', '11:52:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (190, 'MT-2016-000425', '2016-01-02', '11:53:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (191, 'MT-2016-000425', '2016-01-02', '11:53:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (192, 'MT-2016-000426', '2016-01-02', '11:54:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (193, 'MT-2016-000426', '2016-01-02', '11:54:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (194, 'MT-2016-000427', '2016-01-02', '12:03:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (195, 'MT-2016-000427', '2016-01-02', '12:03:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (196, 'MT-2016-000428', '2016-01-02', '12:04:00', 'Arrest', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (197, 'MT-2016-000428', '2016-01-02', '12:04:00', 'Arrest', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (198, 'MT-2016-000428', '2016-01-02', '12:04:00', 'Arrest', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (199, 'MT-2016-000429', '2016-01-02', '12:07:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (200, 'MT-2016-000429', '2016-01-02', '12:07:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (201, 'MT-2016-000430', '2016-01-02', '12:10:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (202, 'MT-2016-000430', '2016-01-02', '12:10:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (203, 'MT-2016-000431', '2016-01-02', '12:10:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (204, 'MT-2016-000432', '2016-01-02', '12:10:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (205, 'MT-2016-000432', '2016-01-02', '12:10:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (206, 'MT-2016-000433', '2016-01-02', '12:14:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (207, 'MT-2016-000434', '2016-01-02', '12:14:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (208, 'MT-2016-000434', '2016-01-02', '12:14:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (209, 'MT-2016-000435', '2016-01-02', '12:23:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (210, 'MT-2016-000436', '2016-01-02', '12:25:00', 'Arrest', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (211, 'MT-2016-000437', '2016-01-02', '12:30:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (212, 'MT-2016-000437', '2016-01-02', '12:30:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (213, 'MT-2016-000438', '2016-01-02', '12:30:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (214, 'MT-2016-000438', '2016-01-02', '12:30:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (215, 'MT-2016-000439', '2016-01-02', '12:32:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (216, 'MT-2016-000440', '2016-01-02', '12:35:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (217, 'MT-2016-000440', '2016-01-02', '12:35:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (218, 'MT-2016-000441', '2016-01-02', '12:35:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (219, 'MT-2016-000442', '2016-01-02', '12:48:00', 'Arrest', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (220, 'MT-2016-000442', '2016-01-02', '12:48:00', 'Arrest', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (221, 'MT-2016-000443', '2016-01-02', '12:49:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (222, 'MT-2016-000443', '2016-01-02', '12:49:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (223, 'MT-2016-000444', '2016-01-02', '12:53:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (224, 'MT-2016-000445', '2016-01-02', '12:56:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (225, 'MT-2016-000445', '2016-01-02', '12:56:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (226, 'MT-2016-000445', '2016-01-02', '12:56:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (227, 'MT-2016-000446', '2016-01-02', '13:07:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (228, 'MT-2016-000447', '2016-01-02', '13:08:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (229, 'MT-2016-000447', '2016-01-02', '13:08:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (230, 'MT-2016-000448', '2016-01-02', '13:10:00', 'Warning', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (231, 'MT-2016-000449', '2016-01-02', '13:11:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (232, 'MT-2016-000449', '2016-01-02', '13:11:00', 'Citation', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (233, 'MT-2016-000449', '2016-01-02', '13:11:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (234, 'MT-2016-000450', '2016-01-02', '13:13:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (235, 'MT-2016-000451', '2016-01-02', '13:18:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (236, 'MT-2016-000452', '2016-01-02', '13:21:00', 'Arrest', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (237, 'MT-2016-000453', '2016-01-02', '13:22:00', 'Citation', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (238, 'MT-2016-000453', '2016-01-02', '13:22:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (239, 'MT-2016-000453', '2016-01-02', '13:22:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (240, 'MT-2016-000454', '2016-01-02', '13:23:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (241, 'MT-2016-000455', '2016-01-02', '13:24:00', 'Warning', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (242, 'MT-2016-000456', '2016-01-02', '13:27:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (243, 'MT-2016-000457', '2016-01-02', '13:30:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (244, 'MT-2016-000458', '2016-01-02', '13:38:00', 'Warning', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (245, 'MT-2016-000459', '2016-01-02', '13:39:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (246, 'MT-2016-000460', '2016-01-02', '13:40:00', 'Arrest', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (247, 'MT-2016-000460', '2016-01-02', '13:40:00', 'Arrest', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (248, 'MT-2016-000461', '2016-01-02', '13:47:00', 'Warning', '1011');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (249, 'MT-2016-000462', '2016-01-02', '13:55:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (250, 'MT-2016-000463', '2016-01-02', '13:56:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (251, 'MT-2016-000463', '2016-01-02', '13:56:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (252, 'MT-2016-000464', '2016-01-02', '13:58:00', 'Citation', '1007');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (253, 'MT-2016-000465', '2016-01-02', '14:00:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (254, 'MT-2016-000465', '2016-01-02', '14:00:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (255, 'MT-2016-000466', '2016-01-02', '14:01:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (256, 'MT-2016-000466', '2016-01-02', '14:01:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (257, 'MT-2016-000467', '2016-01-02', '14:02:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (258, 'MT-2016-000468', '2016-01-02', '14:09:00', 'Warning', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (259, 'MT-2016-000468', '2016-01-02', '14:09:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (260, 'MT-2016-000468', '2016-01-02', '14:09:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (261, 'MT-2016-000469', '2016-01-02', '14:11:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (262, 'MT-2016-000470', '2016-01-02', '14:12:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (263, 'MT-2016-000470', '2016-01-02', '14:12:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (264, 'MT-2016-000471', '2016-01-02', '14:12:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (265, 'MT-2016-000472', '2016-01-02', '14:24:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (266, 'MT-2016-000472', '2016-01-02', '14:24:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (267, 'MT-2016-000473', '2016-01-02', '14:24:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (268, 'MT-2016-000473', '2016-01-02', '14:24:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (269, 'MT-2016-000474', '2016-01-02', '14:24:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (270, 'MT-2016-000475', '2016-01-02', '14:27:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (271, 'MT-2016-000476', '2016-01-02', '14:35:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (272, 'MT-2016-000476', '2016-01-02', '14:35:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (273, 'MT-2016-000477', '2016-01-02', '14:37:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (274, 'MT-2016-000477', '2016-01-02', '14:37:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (275, 'MT-2016-000478', '2016-01-02', '14:40:00', 'Arrest', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (276, 'MT-2016-000478', '2016-01-02', '14:40:00', 'Arrest', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (277, 'MT-2016-000478', '2016-01-02', '14:40:00', 'Arrest', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (278, 'MT-2016-000479', '2016-01-02', '14:41:00', 'Warning', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (279, 'MT-2016-000480', '2016-01-02', '14:42:00', 'Citation', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (280, 'MT-2016-000480', '2016-01-02', '14:42:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (281, 'MT-2016-000480', '2016-01-02', '14:42:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (282, 'MT-2016-000481', '2016-01-02', '14:42:00', 'Warning', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (283, 'MT-2016-000481', '2016-01-02', '14:42:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (284, 'MT-2016-000482', '2016-01-02', '14:44:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (285, 'MT-2016-000482', '2016-01-02', '14:44:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (286, 'MT-2016-000483', '2016-01-02', '14:44:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (287, 'MT-2016-000483', '2016-01-02', '14:44:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (288, 'MT-2016-000483', '2016-01-02', '14:44:00', 'Citation', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (289, 'MT-2016-000484', '2016-01-02', '14:48:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (290, 'MT-2016-000485', '2016-01-02', '14:50:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (291, 'MT-2016-000486', '2016-01-02', '14:53:00', 'Warning', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (292, 'MT-2016-000486', '2016-01-02', '14:53:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (293, 'MT-2016-000486', '2016-01-02', '14:53:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (294, 'MT-2016-000487', '2016-01-02', '14:55:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (295, 'MT-2016-000488', '2016-01-02', '14:57:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (296, 'MT-2016-000488', '2016-01-02', '14:57:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (297, 'MT-2016-000488', '2016-01-02', '14:57:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (298, 'MT-2016-000489', '2016-01-02', '14:58:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (299, 'MT-2016-000490', '2016-01-02', '14:59:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (300, 'MT-2016-000490', '2016-01-02', '14:59:00', 'Warning', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (301, 'MT-2016-000490', '2016-01-02', '14:59:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (302, 'MT-2016-000491', '2016-01-02', '15:04:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (303, 'MT-2016-000491', '2016-01-02', '15:04:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (304, 'MT-2016-000492', '2016-01-02', '15:07:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (305, 'MT-2016-000493', '2016-01-02', '15:07:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (306, 'MT-2016-000493', '2016-01-02', '15:07:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (307, 'MT-2016-000493', '2016-01-02', '15:07:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (308, 'MT-2016-000494', '2016-01-02', '15:08:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (309, 'MT-2016-000494', '2016-01-02', '15:08:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (310, 'MT-2016-000495', '2016-01-02', '15:10:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (311, 'MT-2016-000496', '2016-01-02', '15:10:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (312, 'MT-2016-000497', '2016-01-02', '15:11:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (313, 'MT-2016-000497', '2016-01-02', '15:11:00', 'Warning', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (314, 'MT-2016-000497', '2016-01-02', '15:11:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (315, 'MT-2016-000498', '2016-01-02', '15:12:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (316, 'MT-2016-000498', '2016-01-02', '15:12:00', 'Warning', '1010');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (317, 'MT-2016-000499', '2016-01-02', '15:17:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (318, 'MT-2016-000500', '2016-01-02', '15:22:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (319, 'MT-2016-000500', '2016-01-02', '15:22:00', 'Warning', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (320, 'MT-2016-000501', '2016-01-02', '15:28:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (321, 'MT-2016-000502', '2016-01-02', '15:31:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (322, 'MT-2016-000502', '2016-01-02', '15:31:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (323, 'MT-2016-000503', '2016-01-02', '15:33:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (324, 'MT-2016-000503', '2016-01-02', '15:33:00', 'Citation', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (325, 'MT-2016-000503', '2016-01-02', '15:33:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (326, 'MT-2016-000504', '2016-01-02', '15:35:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (327, 'MT-2016-000504', '2016-01-02', '15:35:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (328, 'MT-2016-000505', '2016-01-02', '15:39:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (329, 'MT-2016-000505', '2016-01-02', '15:39:00', 'Citation', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (330, 'MT-2016-000505', '2016-01-02', '15:39:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (331, 'MT-2016-000506', '2016-01-02', '15:43:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (332, 'MT-2016-000506', '2016-01-02', '15:43:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (333, 'MT-2016-000507', '2016-01-02', '15:44:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (334, 'MT-2016-000508', '2016-01-02', '15:45:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (335, 'MT-2016-000508', '2016-01-02', '15:45:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (336, 'MT-2016-000509', '2016-01-02', '15:47:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (337, 'MT-2016-000510', '2016-01-02', '15:47:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (338, 'MT-2016-000510', '2016-01-02', '15:47:00', 'Warning', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (339, 'MT-2016-000510', '2016-01-02', '15:47:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (340, 'MT-2016-000511', '2016-01-02', '15:57:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (341, 'MT-2016-000511', '2016-01-02', '15:57:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (342, 'MT-2016-000512', '2016-01-02', '15:58:00', 'Warning', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (343, 'MT-2016-000513', '2016-01-02', '16:00:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (344, 'MT-2016-000514', '2016-01-02', '16:02:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (345, 'MT-2016-000514', '2016-01-02', '16:02:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (346, 'MT-2016-000515', '2016-01-02', '16:02:00', 'Citation', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (347, 'MT-2016-000515', '2016-01-02', '16:02:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (348, 'MT-2016-000515', '2016-01-02', '16:02:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (349, 'MT-2016-000516', '2016-01-02', '16:02:00', 'Citation', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (350, 'MT-2016-000516', '2016-01-02', '16:02:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (351, 'MT-2016-000516', '2016-01-02', '16:02:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (352, 'MT-2016-000517', '2016-01-02', '16:03:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (353, 'MT-2016-000517', '2016-01-02', '16:03:00', 'Citation', '1007');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (354, 'MT-2016-000518', '2016-01-02', '16:03:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (355, 'MT-2016-000518', '2016-01-02', '16:03:00', 'Citation', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (356, 'MT-2016-000518', '2016-01-02', '16:03:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (357, 'MT-2016-000519', '2016-01-02', '16:04:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (358, 'MT-2016-000520', '2016-01-02', '16:08:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (359, 'MT-2016-000520', '2016-01-02', '16:08:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (360, 'MT-2016-000521', '2016-01-02', '16:08:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (361, 'MT-2016-000522', '2016-01-02', '16:13:00', 'Warning', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (362, 'MT-2016-000522', '2016-01-02', '16:13:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (363, 'MT-2016-000523', '2016-01-02', '16:15:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (364, 'MT-2016-000524', '2016-01-02', '16:18:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (365, 'MT-2016-000525', '2016-01-02', '16:18:00', 'Citation', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (366, 'MT-2016-000525', '2016-01-02', '16:18:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (367, 'MT-2016-000525', '2016-01-02', '16:18:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (368, 'MT-2016-000526', '2016-01-02', '16:20:00', 'Citation', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (369, 'MT-2016-000526', '2016-01-02', '16:20:00', 'Citation', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (370, 'MT-2016-000526', '2016-01-02', '16:20:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (371, 'MT-2016-000527', '2016-01-02', '16:21:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (372, 'MT-2016-000528', '2016-01-02', '16:21:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (373, 'MT-2016-000529', '2016-01-02', '16:21:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (374, 'MT-2016-000530', '2016-01-02', '16:24:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (375, 'MT-2016-000531', '2016-01-02', '16:28:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (376, 'MT-2016-000532', '2016-01-02', '16:30:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (377, 'MT-2016-000532', '2016-01-02', '16:30:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (378, 'MT-2016-000532', '2016-01-02', '16:30:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (379, 'MT-2016-000533', '2016-01-02', '16:34:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (380, 'MT-2016-000533', '2016-01-02', '16:34:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (381, 'MT-2016-000534', '2016-01-02', '16:35:00', 'Warning', '1006');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (382, 'MT-2016-000535', '2016-01-02', '16:36:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (383, 'MT-2016-000536', '2016-01-02', '16:39:00', 'Citation', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (384, 'MT-2016-000536', '2016-01-02', '16:39:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (385, 'MT-2016-000537', '2016-01-02', '16:41:00', 'Warning', '1007');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (386, 'MT-2016-000537', '2016-01-02', '16:41:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (387, 'MT-2016-000538', '2016-01-02', '16:47:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (388, 'MT-2016-000538', '2016-01-02', '16:47:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (389, 'MT-2016-000538', '2016-01-02', '16:47:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (390, 'MT-2016-000539', '2016-01-02', '16:52:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (391, 'MT-2016-000540', '2016-01-02', '16:52:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (392, 'MT-2016-000541', '2016-01-02', '16:53:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (393, 'MT-2016-000542', '2016-01-02', '16:55:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (394, 'MT-2016-000543', '2016-01-02', '17:04:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (395, 'MT-2016-000544', '2016-01-02', '17:07:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (396, 'MT-2016-000545', '2016-01-02', '17:30:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (397, 'MT-2016-000546', '2016-01-02', '17:32:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (398, 'MT-2016-000546', '2016-01-02', '17:32:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (399, 'MT-2016-000547', '2016-01-02', '17:33:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (400, 'MT-2016-000547', '2016-01-02', '17:33:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (401, 'MT-2016-000548', '2016-01-02', '17:38:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (402, 'MT-2016-000548', '2016-01-02', '17:38:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (403, 'MT-2016-000549', '2016-01-02', '17:40:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (404, 'MT-2016-000549', '2016-01-02', '17:40:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (405, 'MT-2016-000550', '2016-01-02', '17:42:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (406, 'MT-2016-000550', '2016-01-02', '17:42:00', 'Citation', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (407, 'MT-2016-000551', '2016-01-02', '17:43:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (408, 'MT-2016-000552', '2016-01-02', '17:45:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (409, 'MT-2016-000553', '2016-01-02', '17:45:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (410, 'MT-2016-000554', '2016-01-02', '17:46:00', 'Citation', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (411, 'MT-2016-000555', '2016-01-02', '17:46:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (412, 'MT-2016-000556', '2016-01-02', '17:54:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (413, 'MT-2016-000556', '2016-01-02', '17:54:00', 'Citation', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (414, 'MT-2016-000557', '2016-01-02', '17:56:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (415, 'MT-2016-000557', '2016-01-02', '17:56:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (416, 'MT-2016-000557', '2016-01-02', '17:56:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (417, 'MT-2016-000558', '2016-01-02', '17:57:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (418, 'MT-2016-000559', '2016-01-02', '17:57:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (419, 'MT-2016-000559', '2016-01-02', '17:57:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (420, 'MT-2016-000559', '2016-01-02', '17:57:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (421, 'MT-2016-000560', '2016-01-02', '17:59:00', 'Citation', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (422, 'MT-2016-000560', '2016-01-02', '17:59:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (423, 'MT-2016-000560', '2016-01-02', '17:59:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (424, 'MT-2016-000561', '2016-01-02', '18:01:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (425, 'MT-2016-000562', '2016-01-02', '18:02:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (426, 'MT-2016-000563', '2016-01-02', '18:05:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (427, 'MT-2016-000563', '2016-01-02', '18:05:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (428, 'MT-2016-000564', '2016-01-02', '18:06:00', 'Warning', '1011');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (429, 'MT-2016-000565', '2016-01-02', '18:11:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (430, 'MT-2016-000566', '2016-01-02', '18:12:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (431, 'MT-2016-000566', '2016-01-02', '18:12:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (432, 'MT-2016-000567', '2016-01-02', '18:16:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (433, 'MT-2016-000567', '2016-01-02', '18:16:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (434, 'MT-2016-000568', '2016-01-02', '18:19:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (435, 'MT-2016-000569', '2016-01-02', '18:24:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (436, 'MT-2016-000570', '2016-01-02', '18:27:00', 'Warning', '1007');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (437, 'MT-2016-000570', '2016-01-02', '18:27:00', 'Warning', '1010');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (438, 'MT-2016-000571', '2016-01-02', '18:30:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (439, 'MT-2016-000571', '2016-01-02', '18:30:00', 'Citation', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (440, 'MT-2016-000571', '2016-01-02', '18:30:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (441, 'MT-2016-000572', '2016-01-02', '18:31:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (442, 'MT-2016-000572', '2016-01-02', '18:31:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (443, 'MT-2016-000573', '2016-01-02', '18:37:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (444, 'MT-2016-000573', '2016-01-02', '18:37:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (445, 'MT-2016-000574', '2016-01-02', '18:39:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (446, 'MT-2016-000574', '2016-01-02', '18:39:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (447, 'MT-2016-000575', '2016-01-02', '18:45:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (448, 'MT-2016-000576', '2016-01-02', '18:49:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (449, 'MT-2016-000576', '2016-01-02', '18:49:00', 'Citation', '1010');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (450, 'MT-2016-000577', '2016-01-02', '18:51:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (451, 'MT-2016-000578', '2016-01-02', '18:53:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (452, 'MT-2016-000578', '2016-01-02', '18:53:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (453, 'MT-2016-000579', '2016-01-02', '19:00:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (454, 'MT-2016-000579', '2016-01-02', '19:00:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (455, 'MT-2016-000579', '2016-01-02', '19:00:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (456, 'MT-2016-000580', '2016-01-02', '19:00:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (457, 'MT-2016-000581', '2016-01-02', '19:04:00', 'Arrest', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (458, 'MT-2016-000581', '2016-01-02', '19:04:00', 'Arrest', '1004');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (459, 'MT-2016-000582', '2016-01-02', '19:04:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (460, 'MT-2016-000583', '2016-01-02', '19:06:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (461, 'MT-2016-000584', '2016-01-02', '19:06:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (462, 'MT-2016-000585', '2016-01-02', '19:11:00', 'Citation', '1007');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (463, 'MT-2016-000585', '2016-01-02', '19:11:00', 'Citation', '1008');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (464, 'MT-2016-000586', '2016-01-02', '19:13:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (465, 'MT-2016-000586', '2016-01-02', '19:13:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (466, 'MT-2016-000586', '2016-01-02', '19:13:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (467, 'MT-2016-000587', '2016-01-02', '19:17:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (468, 'MT-2016-000587', '2016-01-02', '19:17:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (469, 'MT-2016-000588', '2016-01-02', '19:18:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (470, 'MT-2016-000588', '2016-01-02', '19:18:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (471, 'MT-2016-000589', '2016-01-02', '19:21:00', 'Warning', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (472, 'MT-2016-000589', '2016-01-02', '19:21:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (473, 'MT-2016-000589', '2016-01-02', '19:21:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (474, 'MT-2016-000590', '2016-01-02', '19:26:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (475, 'MT-2016-000590', '2016-01-02', '19:26:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (476, 'MT-2016-000590', '2016-01-02', '19:26:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (477, 'MT-2016-000591', '2016-01-02', '19:28:00', 'Warning', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (478, 'MT-2016-000591', '2016-01-02', '19:28:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (479, 'MT-2016-000591', '2016-01-02', '19:28:00', 'Warning', '1007');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (480, 'MT-2016-000592', '2016-01-02', '19:33:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (481, 'MT-2016-000593', '2016-01-02', '19:33:00', 'Warning', '1001');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (482, 'MT-2016-000593', '2016-01-02', '19:33:00', 'Warning', '1007');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (483, 'MT-2016-000594', '2016-01-02', '19:33:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (484, 'MT-2016-000595', '2016-01-02', '19:37:00', 'Warning', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (485, 'MT-2016-000596', '2016-01-02', '19:43:00', 'Warning', '1010');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (486, 'MT-2016-000597', '2016-01-02', '19:45:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (487, 'MT-2016-000597', '2016-01-02', '19:45:00', 'Citation', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (488, 'MT-2016-000598', '2016-01-02', '19:46:00', 'Citation', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (489, 'MT-2016-000598', '2016-01-02', '19:46:00', 'Citation', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (490, 'MT-2016-000598', '2016-01-02', '19:46:00', 'Citation', '1010');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (491, 'MT-2016-000599', '2016-01-02', '19:46:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (492, 'MT-2016-000600', '2016-01-02', '19:54:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (493, 'MT-2016-000601', '2016-01-02', '19:56:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (494, 'MT-2016-000601', '2016-01-02', '19:56:00', 'Warning', '1010');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (495, 'MT-2016-000602', '2016-01-02', '19:56:00', 'Citation', '1009');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (496, 'MT-2016-000603', '2016-01-02', '19:57:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (497, 'MT-2016-000604', '2016-01-02', '19:57:00', 'Warning', '1002');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (498, 'MT-2016-000604', '2016-01-02', '19:57:00', 'Warning', '1003');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (499, 'MT-2016-000604', '2016-01-02', '19:57:00', 'Warning', '1005');
INSERT INTO public.stop_info (record_id, stop_id, stop_date, stop_time, stop_outcome, violation_id) VALUES (500, 'MT-2016-000605', '2016-01-02', '19:58:00', 'Warning', '1009');


--
-- TOC entry 3829 (class 0 OID 16506)
-- Dependencies: 199
-- Data for Name: vehicle_details; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000313', '1997', 'OLDSMOBILE (OLD', '88', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000314', '2004', 'HONDA', 'CIVIC', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000315', '2005', 'NISSAN (NISS)', 'MURANO', 'CROSSOVER');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000316', '1990', 'DODGE', 'RAM', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000317', '1995', 'CHEVROLET', 'C2500', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000318', '1995', 'CHEVROLET (CHEV', 'C25', 'Pickup');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000319', '1996', 'ITASCA', 'SUNCRUISER', 'COACH');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000320', '2001', 'CHEVROLET (CHEV', 'SLV', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000321', '2004', 'BUICK (BUIC)', 'LESABRE', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000322', '1994', 'CHEVROLET (CHEV', 'C15', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000323', '1993', 'CHEV', 'Burban', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000324', '1995', 'CHEVROLET (CHEV', 'CORSICA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000325', '2005', 'CHEVROLET (CHEV', 'IMPALA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000326', '2015', 'GMC (GMC)', 'TERRAIN', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000327', '1995', 'SUBARU (SUBA)', 'LEGACY', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000328', '2000', 'CADILLAC (CADI)', 'SEVILLE', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000329', '1986', 'TOYOTA (TOYT)', 'SR5', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000330', '2017', 'CHEV', 'Cruz', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000331', '2006', 'TOYOTA (TOYT)', 'COA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000332', '2004', 'FORD (FORD)', 'F35', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000333', '2005', 'VOLKS', 'JETTA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000334', '2007', 'CHEVROLET', 'K2', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000335', '1993', 'CHEVROLET (CHEV', 'SUB', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000336', '2013', 'SUBARU (SUBA)', 'IMPREZA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000337', '2003', 'GMC (GMC)', 'YUKON', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000338', '1999', 'TOYOTA (TOYT)', 'TAC', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000339', '1997', 'JEEP (JEEP)', 'CHE', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000340', '1999', 'PREVOST', 'H3-45', 'BUS');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000341', '2008', 'DODGE (DODG)', 'GRAND', 'VAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000342', '2014', 'JEEP', 'WRANGLER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000343', '2014', 'SUBAR', 'FORESTER', 'CROSSOVER');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000344', '1998', 'LAND', 'DISCOVERY', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000345', '2016', 'CHEVROLET (CHEV', 'EQUINOX', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000346', '1997', 'CHEVROLET (CHEV', 'LUM', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000347', '2004', 'TOYOTA (TOYT)', 'TUN', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000348', '2015', 'GMC', 'S15 PICKUP', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000349', '1997', 'FORD (FORD)', 'MUSTANG', 'COUPE');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000350', '2007', 'FORD (FORD)', 'F15', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000351', '2015', 'TOYOTA', 'CAMRY', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000352', '2014', 'DODGE (DODG)', 'DURANGO', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000353', '2010', 'GMC (GMC)', 'SIERRA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000354', '2008', 'PONTIAC (PONT)', 'GRAND', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000355', '2012', 'NISSAN (NISS)', 'XTERRA', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000356', '1990', 'HOND', '3 wheeler', 'ATV');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000357', '2007', 'GMC (GMC)', 'SIERRA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000358', '2016', 'TOYOTA (TOYT)', 'TUNDRA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000359', '2004', 'TOYOTA (TOYT)', '4RN', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000360', '2015', 'FORD (FORD)', 'F350', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000361', '2003', 'CHEVROLET (CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000362', '2012', 'HONDA (HOND)', 'ACCORD', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000363', '2012', 'CADILLAC (CADI)', 'SRX', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000364', '2015', 'CHEV', 'SUBURBAN', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000365', '2013', 'RAM', '3500', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000366', '2008', 'CHEV', 'SUBURBAN', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000367', '2005', 'INFINITI (INFI)', 'G35', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000368', '2004', 'TOYOTA (TOYT)', 'CAMRY', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000369', '2006', 'CHEVROLET (CHEV', 'COBALT', 'COUPE');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000370', '2011', 'HONDA (HOND)', 'CR-V', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000371', '2008', 'DODGE (DODG)', 'R15', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000372', '2007', 'FORD (FORD)', 'TAURUS', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000373', '2013', 'NISSAN (NISS)', 'ALTIMA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000374', '2012', 'DODGE (DODG)', 'RAM', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000375', '2011', 'CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000376', '2013', 'FORD (FORD)', 'F150', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000377', '2013', 'TOYOT', 'COROLLA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000378', '2016', 'HYUN', 'SANTA FE', 'CROSSOVER');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000379', '2011', 'TOYOT', 'CAMRY', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000380', '1978', 'FORD', 'F250 PICKUP', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000381', '1996', 'INFINITI (INFI)', 'I30', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000382', '2006', 'LINCOLN (LINC)', 'NAVIGATOR', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000383', '2007', 'FORD', 'EXPEDITION', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000384', '2015', 'SUBARU (SUBA)', 'XV', 'HATCHBACK');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000385', '2003', 'BMW (BMW)', 'X5', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000386', '2014', 'FORD (FORD)', 'F350', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000387', '2014', 'DODG', 'RAM', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000388', '2013', 'FORD', 'F350 PICKUP', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000389', '2015', 'TOYOTa', 'Sienna', 'VAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000390', '1984', 'MERCEDES-BENZ (', '300', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000391', '2010', 'TOYOTA (TOYT)', 'COROLLA/S/LE/XL', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000392', '2005', 'CHEVROLET (CHEV', 'IMPALA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000393', '2011', 'TOYOTA (TOYT)', 'RAV4', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000394', '2011', 'NISS', 'ALTIMA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000395', '2016', 'ACURA (ACUR)', 'MDX', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000396', '2008', 'CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000397', '2008', 'PONT', 'G5', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000398', '2003', 'FORD (FORD)', 'ESCORT', 'COUPE');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000399', '2002', 'DODGE (DODG)', 'NEON', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000400', '2015', 'DODGe', 'Ram', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000401', '2014', 'CHEVROLET (CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000402', '2011', 'SUBARU', 'Legacy', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000403', '2013', 'DODG', 'RAM', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000404', '2008', 'HUMM', 'H3', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000405', '2015', 'CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000406', '1996', 'HONDA (HOND)', 'ACC', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000407', '2013', 'FORD', 'F150 PICKUP', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000408', '2005', 'CHEVROLET (CHEV', 'SUB', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000409', '2003', 'TOYOTA (TOYT)', 'SEQ', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000410', '2014', 'GMC', 'SIERRA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000411', '2004', 'CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000412', '1997', 'FORD', 'ASPIRE', 'HATCHBACK');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000413', '2011', 'CADI', 'Escalade', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000414', '1994', 'MERCURY (MERC)', 'MARQUIS', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000415', '2005', 'JEEP (JEEP)', 'LBY', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000416', '2012', 'FORD', 'ESCAPE', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000417', '2008', 'CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000418', '2001', 'HONDA', 'CIVIC', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000419', '1969', 'CHEVROLET (CHEV', 'NOV', 'COUPE');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000420', '2011', 'CHEVY', 'TRAVERSE', 'CROSSOVER');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000421', '2002', 'GMC (GMC)', 'SON', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000422', '2002', 'CHEVROLET (CHEV', 'CAVALIER', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000423', '2008', 'GMC', 'YUKON', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000424', '2011', 'CHEVROLET (CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000425', '2016', 'FORD (FORD)', 'EXPLORER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000426', '2014', 'GMC', 'Yukon', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000427', '2004', 'GMC (GMC)', 'YKN', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000428', '1997', 'HONDA (HOND)', 'CIVIC', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000429', '2015', 'CHEVROLET (CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000430', '2005', 'BUICK (BUIC)', 'RAN', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000431', '2015', 'SUBAR', 'OUTBACK', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000432', '2000', 'CHEVROLET (CHEV', 'SUB', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000433', '2012', 'CHRY', '200', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000434', '2009', 'CHEVROLET (CHEV', 'SLV', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000435', '2000', 'DODG', 'DAKOTA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000436', '2007', 'TOYOTA', 'HIGHLANDER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000437', '1993', 'BUICK (BUIC)', 'PAR', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000438', '2007', 'GMC (GMC)', 'YUKON', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000439', '2003', 'GMC', 'Sra', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000440', '2007', 'TOYOTA (TOYT)', 'TUN', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000441', '2011', 'GENERAL MOTORS', 'GAS', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000442', '2006', 'SUBAR', 'FORESTER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000443', '2004', 'CHEVROLET (CHEV', 'SLV', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000444', '2003', 'FORD (FORD)', 'ECP', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000445', '2001', 'DODGE (DODG)', 'DAKOTA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000446', '2014', 'SUBAR', 'FORESTER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000447', '1999', 'DODGE (DODG)', 'DAKOTA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000448', '2001', 'CHEVROLET (CHEV', 'IMPALA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000449', '2002', 'CHEVROLET (CHEV', 'IMPALA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000450', '2014', 'GMC', 'SIERRA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000451', '2013', 'FORD (FORD)', 'EXPLORER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000452', '2005', 'DODGE', 'RAM 1500', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000453', '1994', 'FORD (FORD)', 'F25', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000454', '2008', 'TOYOTA (TOYT)', 'TACOMA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000455', '1998', 'FORD (FORD)', 'CNT', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000456', '2015', 'DODG', 'RAM', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000457', '2015', 'SUBAR', 'crosstrek', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000458', '1999', 'SUBARU (SUBA)', 'IMPREZA', 'HATCHBACK');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000459', '1997', 'MERCURY (MERC)', 'TRA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000460', '2010', 'CHEVROLET (CHEV', 'TRAVERSE', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000461', '2015', 'FREIGHTLINER', NULL, 'CONVENTIONAL TRUCK');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000462', '2011', 'FORD (FORD)', 'F350', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000463', '2014', 'CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000464', '2001', 'CHEVROLET (CHEV', 'SLV', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000465', '2005', 'PONTIAC (PONT)', 'GRAND', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000466', '1995', 'HONDA (HOND)', 'ACC', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000467', '2013', 'NISS', 'ALTIMA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000468', '2004', 'GMC (GMC)', 'ENV', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000469', '2013', 'FORD', 'F150 PICKUP', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000470', '2007', 'CHRYSLER (CHRY)', '300', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000471', '2014', 'CHEVROLET (CHEV', 'TRAVERSE', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000472', '2007', 'CHEVROLET', 'K2500', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000473', '2005', 'CHEVROLET (CHEV', 'EQUINOX', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000474', '2007', 'MITS', 'ENDEAVOR', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000475', '2011', 'CHEVROLET (CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000476', '1986', 'FORD (FORD)', 'F25', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000477', '2011', 'NISSAN (NISS)', 'FRONTIER', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000478', '1996', 'DODGE (DODG)', 'DAKOTA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000479', '1995', 'FORD (FORD)', 'CNT', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000480', '2005', 'CHEVROLET (CHEV', 'AVA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000481', '2013', 'FORD', 'F150 PICKUP', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000482', '2007', 'GMC', 'SIERRA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000483', '1986', 'FORD', 'F250 PICKUP', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000484', '2014', 'CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000485', '2001', 'TOYOT', 'COROLLA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000486', '2011', 'DODGE (DODG)', 'DURANGO', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000487', '2010', 'CHEVROLET (CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000488', '2013', 'VOLKSWAGEN (VOL', 'JETTA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000489', '1993', 'DODGE (DODG)', 'XXX', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000490', '2006', 'LAND', 'RANGE ROVER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000491', '2014', 'FORD', 'ESCAPE', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000492', '2003', 'DODG', 'RAM', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000493', '1998', 'DODGE (DODG)', 'DAKOTA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000494', '1976', 'FORD (FORD)', 'TK', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000495', '2009', 'CHEVROLET (CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000496', '2013', 'CHEVROLET', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000497', '1992', 'FORD (FORD)', 'F35', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000498', '2000', 'CHRYSLER (CHRY)', 'VOY', 'VAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000499', '2014', 'HONDA (HOND)', 'CR-V', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000500', '2001', 'SUBAR', 'OUTBACK', 'WAGON');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000501', '2001', 'CHEVROLET', 'CAVALIER', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000502', '2007', 'GMC (GMC)', 'NEW', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000503', '2005', 'DODGE', 'R1500', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000504', '2003', 'SUBARU (SUBA)', 'LEGACY', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000505', '1998', 'DODGE', 'DAKOTA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000506', '2015', 'BUICK (BUIC)', 'ENCORE', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000507', '2006', 'TOYOTA (TOYT)', 'TUN', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000508', '2005', 'DODGE (DODG)', 'R15', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000509', '1999', 'HONDA', 'ACCORD', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000510', '2002', 'VOLVO (VOLV)', 'S60', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000511', '2005', 'FORD (FORD)', 'TAURUS', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000512', '2010', 'GMC (GMC)', 'TERRAIN', 'CROSSOVER');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000513', '2012', 'SUBARU (SUBA)', 'FORESTER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000514', '2013', 'RAM', 'RAM', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000515', '2006', 'JEEP', 'COMMANDER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000516', '2006', 'JEEP', 'COMMANDER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000517', '1995', 'GMC (GMC)', 'YKN', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000518', '2000', 'PONTIAC (PONT)', 'BONNEVILLE', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000519', '2014', 'FIAT', '500', 'WAGON');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000520', '1999', 'FORD', 'WINDSTAR', 'VAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000521', '2009', 'FREightliner', NULL, 'CONVENTIONAL TRUCK');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000522', '1990', 'CHEVROLET (CHEV', 'C35', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000523', '2003', 'CHEV', 'SILVERADO', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000524', '2008', 'DODG', 'RAM', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000525', '2010', 'TOYOTA (TOYT)', 'HIGHLANDER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000526', '2006', 'CHEVROLET', 'HHR', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000527', '2016', 'GMC', 'YUKON', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000528', '2008', 'DODGE (DODG)', 'CHARGER', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000529', '2014', 'FORD', 'TAURUS', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000530', '2012', 'FORD', 'F150 PICKUP', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000531', '1998', 'OLDSMOBILE (OLD', 'CUTLASS', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000532', '2005', 'PONTIAC', 'GRAND PRIX', 'COUPE');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000533', '2009', 'PONTIAC (PONT)', 'G5', 'COUPE');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000534', '2002', 'DODGE (DODG)', 'DAKOTA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000535', '2004', 'FORD', 'FOCUS', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000536', '2006', 'FORD (FORD)', 'FREESTAR', 'VAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000537', '2015', 'GMC', 'ACADIA', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000538', '1995', 'GEO (GEO)', 'PRIZM/LSI', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000539', '2012', 'DODGE (DODG)', 'RAM', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000540', '2015', 'CHEV', 'EQUINOX', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000541', '2015', 'NISSAN', 'ALTIMA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000542', '2012', 'DODG', NULL, NULL);
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000543', '2011', 'DODGE (DODG)', 'NITRO', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000544', '2004', 'DODGE (DODG)', 'R25', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000545', '2016', 'FORD', 'ESCAPE', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000546', '1991', 'CHEVROLET (CHEV', 'C15', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000547', '2005', 'AUDI (AUDI)', 'A6', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000548', '2005', 'SUBARU (SUBA)', 'LEGACY', 'WAGON');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000549', '1984', 'ODES (ODES)', 'ODDESY', 'VAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000550', '1984', 'NISSAN (NISS)', 'XXX', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000551', '2001', 'DODGE (DODG)', 'RAM', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000552', '2010', 'CHRYSLER (CHRY)', 'TOWN', 'VAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000553', '2008', 'CHEVROLET (CHEV', 'MALIBU', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000554', '2000', 'HOND', NULL, 'VAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000555', '1996', 'NISSAN (NISS)', '200SX/SE', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000556', '2000', 'OLDSMOBILE (OLD', 'INTRIGUE', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000557', '1993', 'CHEVROLET (CHEV', 'XXX', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000558', '1999', 'GMC (GMC)', 'JIM', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000559', '2005', 'CHEVROLET (CHEV', 'SLV', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000560', '2004', 'SUBARU (SUBA)', 'LEGACY', 'WAGON');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000561', '2002', 'SUBARU (SUBA)', 'IMPREZA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000562', '2014', 'GMC (GMC)', 'TERRAIN', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000563', '2012', 'CHEV', 'SUBURBAN', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000564', '2016', 'FREightliner', NULL, NULL);
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000565', '2012', 'CHEVROLET (CHEV', 'IMPALA', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000566', '2007', 'DODGE (DODG)', 'CAR', 'VAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000567', '2006', 'FORD (FORD)', 'ECP', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000568', '2005', 'CHEVROLET (CHEV', 'SLV', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000569', '2006', 'CHRYSLER (CHRY)', '300', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000570', '2006', 'NISSAN (NISS)', 'FRONTIER', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000571', '1986', 'VOLVO (VOLV)', '240', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000572', '2005', 'CHEVROLET (CHEV', 'CAVALIER', 'COUPE');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000573', '2012', 'DODGE (DODG)', 'RAM', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000574', '2003', 'TOYOTA (TOYT)', 'SR5', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000575', '2005', 'DODG', 'MAGNUM', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000576', '1989', 'CHEVROLET (CHEV', 'XXX', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000577', '2011', 'FORD (FORD)', 'FUSION', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000578', '1989', 'TOYOTA (TOYT)', 'TERCEL', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000579', '2008', 'GMC', 'ACADIA', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000580', '2013', 'VOLKSWAGEN (VOL', 'PASSAT', 'Sedan 4 Door');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000581', '1999', 'FORD', 'WINDSTAR', 'VAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000582', '2007', 'CHEVROLET (CHEV', 'SLV', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000583', '2008', 'HONDA (HOND)', 'ACC', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000584', '2008', 'FORD (FORD)', 'EDGE', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000585', '1995', 'FORD (FORD)', 'F15', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000586', '1995', 'TOYOTA (TOYT)', '4-RUNNER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000587', '2000', 'CHEVROLET (CHEV', 'SLV', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000588', '2012', 'TOYOTA (TOYT)', 'TUNDRA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000589', '2002', 'TOYOT', 'AVALON', NULL);
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000590', '2002', 'HONDA', 'ACCORD', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000591', '1992', 'CHEVROLET (CHEV', 'LUMINA', 'Sedan 2 Door');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000592', '2015', 'TOYOTA (TOYT)', 'RAV4', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000593', '2008', 'HYUNDAI (HYUN)', 'TIBURON', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000594', NULL, 'SUBAR', 'FORESTER', 'WAGON');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000595', '2000', 'TOYOTA (TOYT)', 'TUNDRA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000596', '1996', 'CHEVROLET (CHEV', 'LUM', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000597', '1992', 'BUICk', 'cen', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000598', '2005', 'BUICK (BUIC)', 'TER', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000599', '2007', 'CHRY', 'SEBRING', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000600', '2005', 'CHRYSLER', 'SEBRING', 'CONVERTIBLE');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000601', '2008', 'CHEVROLET (CHEV', 'K1500', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000602', '2009', 'SUBARu', 'legacy', 'SEDAN');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000603', '2002', 'CHEVROLET (CHEV', 'K1500', 'SPORT UTILITY');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000604', '2014', 'GMC', 'SIERRA', 'PICKUP');
INSERT INTO public.vehicle_details (stop_id, vehicle_year, vehicle_make, vehicle_model, vehicle_style) VALUES ('MT-2016-000605', '2011', 'FORD (FORD)', 'EDGE', 'SPORT UTILITY');


--
-- TOC entry 3830 (class 0 OID 16509)
-- Dependencies: 200
-- Data for Name: violation_details; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.violation_details (violation_id, violation_type) VALUES ('1000', 'DUI');
INSERT INTO public.violation_details (violation_id, violation_type) VALUES ('1001', 'Equipment');
INSERT INTO public.violation_details (violation_id, violation_type) VALUES ('1002', 'License');
INSERT INTO public.violation_details (violation_id, violation_type) VALUES ('1003', 'Lights');
INSERT INTO public.violation_details (violation_id, violation_type) VALUES ('1004', 'Other');
INSERT INTO public.violation_details (violation_id, violation_type) VALUES ('1005', 'Paperwork');
INSERT INTO public.violation_details (violation_id, violation_type) VALUES ('1006', 'Registration/plates');
INSERT INTO public.violation_details (violation_id, violation_type) VALUES ('1007', 'Safe movement');
INSERT INTO public.violation_details (violation_id, violation_type) VALUES ('1008', 'Seat belt');
INSERT INTO public.violation_details (violation_id, violation_type) VALUES ('1009', 'Speeding');
INSERT INTO public.violation_details (violation_id, violation_type) VALUES ('1010', 'Stop sign/light');
INSERT INTO public.violation_details (violation_id, violation_type) VALUES ('1011', 'Truck');


--
-- TOC entry 3838 (class 0 OID 0)
-- Dependencies: 198
-- Name: stop_info_record_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.stop_info_record_id_seq', 1, false);


--
-- TOC entry 3697 (class 2606 OID 16517)
-- Name: stop_info stop_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stop_info
    ADD CONSTRAINT stop_info_pkey PRIMARY KEY (record_id);


--
-- TOC entry 3700 (class 2606 OID 16519)
-- Name: vehicle_details vehicle_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.vehicle_details
    ADD CONSTRAINT vehicle_details_pkey PRIMARY KEY (stop_id);


--
-- TOC entry 3702 (class 2606 OID 16521)
-- Name: violation_details violation_details_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.violation_details
    ADD CONSTRAINT violation_details_pkey PRIMARY KEY (violation_id);


--
-- TOC entry 3695 (class 1259 OID 16522)
-- Name: stop_id_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX stop_id_index ON public.stop_info USING btree (stop_id);


--
-- TOC entry 3698 (class 1259 OID 16523)
-- Name: violation_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX violation_type_index ON public.stop_info USING btree (violation_id);


--
-- TOC entry 3703 (class 2606 OID 16524)
-- Name: stop_info stop_info_stop_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stop_info
    ADD CONSTRAINT stop_info_stop_id_fkey FOREIGN KEY (stop_id) REFERENCES public.vehicle_details(stop_id);


--
-- TOC entry 3704 (class 2606 OID 16529)
-- Name: stop_info stop_info_violation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.stop_info
    ADD CONSTRAINT stop_info_violation_id_fkey FOREIGN KEY (violation_id) REFERENCES public.violation_details(violation_id);


--
-- TOC entry 3836 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM rdsadmin;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2019-10-31 17:43:59

--
-- PostgreSQL database dump complete
--

