# aws_tsopp

Data for this prject is found at: https://data.world/dataneer/tsopp-mt. 

Sign up for AWS Educate: https://aws.amazon.com/education/awseducate/ (It takes about 24 hours for the account to be approved)

The project writeups are going to be done via Google Docs for collaboration: 

- Project proposal: https://docs.google.com/document/d/1_0-Hg81IUpV-V-gLAWni15maz2H2SpRA-6AmsdgSQaQ/edit?usp=sharing


Resources to help us get going:

- https://aws.amazon.com/blogs/big-data/from-sql-to-microservices-integrating-aws-lambda-with-relational-databases/

- How to normalize data into 3NF: https://dba.stackexchange.com/questions/133896/how-to-put-the-below-data-in-3nf 

- All the AWS features available with AWS Educate: https://s3.amazonaws.com/awseducate-starter-account-services/AWS_Educate_Starter_Accounts_and_AWS_Services.pdf

- Configuring AuroaDB with MySQL [10 minute tutorial](https://aws.amazon.com/getting-started/tutorials/configure-connect-serverless-mysql-database-aurora/?sc_icampaign=Adoption_Campaign_pac_q3_919_sitemerch_ribbon_EntApps&sc_ichannel=ha&sc_icontent=awssm-2921&sc_ioutcome=PaaS_Digital_Marketing&sc_iplace=banner&trk=ha_a131L0000058CNMQA2~ha_awssm-2921&trkCampaign=pac_0919_ribbon_gettingstarted_serverless_entapps
)

- [Getting Started with AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/getting-started.html)

- [How to Build a Serverless Web App](https://aws.amazon.com/getting-started/projects/build-serverless-web-app-lambda-apigateway-s3-dynamodb-cognito/) Here we would just need to replace DynamoDB with AuroraDB

- Getting started with React: https://www.youtube.com/watch?v=Ke90Tje7VS0

- Example Dynamic React Table: https://codesandbox.io/s/github/tannerlinsley/react-table/tree/master/archives/v6-examples/react-table-custom-filtering
